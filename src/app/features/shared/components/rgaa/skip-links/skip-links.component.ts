import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-skip-links',
	templateUrl: './skip-links.component.html',
	styleUrls: ['./skip-links.component.scss'],
})
export class SkipLinksComponent implements OnInit {
	@Input() optionnal_step: boolean = false;

	constructor() {}

	ngOnInit(): void {}
}
