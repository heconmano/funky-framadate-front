# 2021
![landing_page](../img/landing_page.png)

## Septembre
Début des jours ouvrés par Tykayn avec les designs de la DINUM
## Mars 
la DINUM propose de contribuer à financer le développement de la nouvelle version.

## Avril
### 19 Avr 
Migration fonctionnelle depuis la BDD de la v1 dans la partie backend


## Octobre
### 5 Oct
le lien vers le lieu où sont travaillées les maquettes de la DINUM https://www.sketch.com/s/5833607c-e93e-4e9b-9c7e-0614238c6d8c

présentation des wireframes
parcours complet, apparence mobile
mobile first
barre de progression, étapes 1 sur 5

comment se présente la gestion des horaires identiques pour chaque jour

* demander aux admins de framadate quels sont les horaires les plus fréquentes dans les sondages de date
* sondage texte: proposer une url d'image ou une URL avec chaque choix de réponse
* délai expiration de 180 jours par défaut
* proposer de participer au sondage après sa création

questions , propositions
* présenter les dates les plus populaires avec une vue calendrier ?
* comment se présente l'écran de vote sur mobile?
* quelle mise en forme des images et des url
* proposer de faire de l'hébergement d'image locale
* mise en forme des plages horaires pour voir qu'elles sont un sous ensemble d'une date
* quoi mettre dans le header et footer
* mise en avant de la destination des données et du droit qui les régit
* personnalisation super admin

### 7 Oct
ici une démo naviguable https://www.sketch.com/s/5833607c-e93e-4e9b-9c7e-0614238c6d8c/a/rbYngRP/play

icones avec feather icons ou fork awesome pour se séparer des icones google

faire une page galerie de composants et typographie. la démo comporte une page prête à l'emploi pour ça. on va pouvoir mettre à jour les variables scss de couleur.

* points de réflexion
	* affichage du footer, menu, logo
	* teinte de gris sur les jours non activables
	* vérifier les contrastes avec tanaguru
	* lien de démo maquette https://www.sketch.com/s/5833607c-e93e-4e9b-9c7e-0614238c6d8c/a/rbYngRP/play
	

### 14 Oct
quelques notes de la démo du jour: * démo du parcours avec un logo

* ajout de définition de la date retenue par le créateur du sondage
* comment proposer du support, des retours, de la contribution
* réponse à un sondage, moyen de dire ok/rien pour toutes les dates
* comment proposer les traductions
* espace pour personnaliser le footer, les liens de support, le titre, les CGU
* accessibilité des graphiques de résultats
* quel affichage pour un champ en erreur, message d'explication

## Novembre
