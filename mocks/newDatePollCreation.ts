export default {
	allow_comments: true,
	allowComments: true,
	allowed_answers: [],
	areResultsPublic: true,
	choices: [],
	choices_grouped: [],
	comments: [],
	creation_date: '2021-05-18T20:40:51.385Z',
	custom_url: 'sdfsfdsfg',
	dateChoices: [
		{
			date_object: '2021-05-19T20:40:18.500Z',
			literal: '2021-05-19',
			timeSlices: {},
		},
		{
			date_object: '2021-05-20T20:40:18.500Z',
			literal: '2021-05-20',
			timeSlices: {},
		},
		{
			date_object: '2021-05-21T20:40:18.500Z',
			literal: '2021-05-21',
			timeSlices: {},
		},
		{
			date_object: '2021-05-22T20:40:18.500Z',
			literal: '2021-05-22',
			timeSlices: {},
		},
		{
			date_object: '2021-05-18T20:40:33.557Z',
			literal: '',
			timeSlices: [],
		},
		{
			date_object: '2021-05-18T20:40:33.557Z',
			literal: '',
			timeSlices: [],
		},
		{
			date_object: '2021-05-18T20:40:33.557Z',
			literal: '',
			timeSlices: [],
		},
	],
	default_expiracy_days_from_now: 60,
	description: 'RSVP',
	has_several_hours: false,
	hasSeveralHours: false,
	id: 0,
	is_zero_knowledge: false,
	isMaybeAnswerAvailable: false,
	isOwnerNotifiedByEmailOnNewComment: false,
	isOwnerNotifiedByEmailOnNewVote: false,
	kind: 'date',
	max_count_of_answers: true,
	maxCountOfAnswers: 150,
	modification_policy: 'everybody',
	owner: {
		email: 'example@example.com',
		polls: [],
		pseudo: 'pseudo',
	},
	password: '',
	stacks: [],
	timeSlices: [
		{
			literal: 'matin',
		},
		{
			literal: 'midi',
		},
		{
			literal: 'après-midi',
		},
		{
			literal: 'soir',
		},
	],
	title: 'sdfsfdsfg',
	votes: [],
};
